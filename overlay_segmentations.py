import numpy as np
import matplotlib.pyplot as plt
import os
import argparse
#  from joblib import Parallel, delayed
from label import load_dicom

def plot_overlay(img, mask, alpha=.3):
    fig, ax = plt.subplots(1)
    ax.imshow(img, cmap='gray')
    ax.imshow(mask, cmap='tab10', alpha=alpha)
    ax.set_axis_off()
    return fig, ax

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--image_dir', required=True,
                        help='path to folder containing DICOM images')
    parser.add_argument('--label_dir', required=True,
                        help='path to folder containing labels in .npy format')
    parser.add_argument('--output_dir', required=True,
                        help='path to folder where the generated overlays will be saved')
    #  parser.add_argument('--n_jobs', required=False, type=int, default=-1,
                        #  help='number of threads to use for image processing (maximum available by default)')
    a = parser.parse_args()
    
    for img_dir, label_dir in zip(os.scandir(a.image_dir), os.scandir(a.label_dir)): 
        imgs, fnames = load_dicom(img_dir.path)
        labels = [np.load(f.path) for f in os.scandir(label_dir)]

        out_dir = os.path.join(a.output_dir, img_dir.name) 
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        for i, m, f in zip(imgs, labels, fnames):
            fig, _ = plot_overlay(i, m)
            fig.savefig(os.path.join(out_dir, os.path.splitext(f.name)[0] + '.png'))
            plt.close(fig)


if __name__ == '__main__':
    main()
